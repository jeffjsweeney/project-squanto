%{
// $Id: scanner.l,v 1.8 2012-10-11 18:22:20-07 - - $

#include <stdlib.h>
#include <stdbool.h>

#include "auxlib.h"
#include "lyutils.h"

#define YY_USER_ACTION  { scanner_useraction (); }
#define IGNORE(THING)   { }

%}

%option 8bit
%option backup
%option debug
%option ecs
%option nodefault
%option nounput
%option perf-report
%option verbose
%option noyywrap

LETTER          [A-Za-z_]
DIGIT           [0-9]
IDENT           ({LETTER}({LETTER}|{DIGIT})*)
INTCONST        ({DIGIT}+)
CHARCONST      ('([^\\'\n]|\\[\\'"0nt])')
STRINGCONST     (\"([^\\"\n]|\\[\\'"0nt])*\")
INVALIDIDENT    ({DIGIT}({LETTER}+))
UNTERMCHAR      ('([^\\'\n]|\\[\\'"0nt]))
UNTERMSTR      (\"([^\\"\n]|\\[\\'"0nt])*)

%%

"#".*           { scanner_include(); }
[ \t]+          { IGNORE (white space) }
\n              { scanner_newline(); }

"="             { return yylval_token ('='); }
"+"             { return yylval_token ('+'); }
"-"             { return yylval_token ('-'); }
"*"             { return yylval_token ('*'); }
"/"             { return yylval_token ('/'); }
"^"             { return yylval_token ('^'); }
"("             { return yylval_token ('('); }
")"             { return yylval_token (')'); }
";"             { return yylval_token (';'); }
"["          { return yylval_token ('['); }
"]"          { return yylval_token (']'); }
"{"          { return yylval_token ('{'); }
"}"          { return yylval_token ('}'); }
","          { return yylval_token (','); }
"."          { return yylval_token ('.'); }
"!"          { return yylval_token ('!'); }
"[]"            { return yylval_token (TOK_ARRAY); }

">="            { return yylval_token (TOK_GE); }
"<="            { return yylval_token (TOK_LE); }
"=="            { return yylval_token (TOK_EQ); }
"<"                { return yylval_token (TOK_LT); }
">"                { return yylval_token (TOK_GT); }
"!="            { return yylval_token (TOK_NE); }




"void"            { return yylval_token (TOK_VOID); }
"bool"            { return yylval_token (TOK_BOOL); }
"char"            { return yylval_token (TOK_CHAR); }
"int"            { return yylval_token (TOK_INT); }
"string"        { return yylval_token (TOK_STRING); }
"struct"        { return yylval_token (TOK_STRUCT); }
"if"            { return yylval_token (TOK_IF); }
"else"            { return yylval_token (TOK_ELSE); }
"while"            { return yylval_token (TOK_WHILE); }
"return"        { return yylval_token (TOK_RETURN); }
"new"            { return yylval_token (TOK_NEW); }
"false"            { return yylval_token (TOK_FALSE); }
"true"            { return yylval_token (TOK_TRUE); }
"null"            { return yylval_token (TOK_NULL); }
"ord"            { return yylval_token (TOK_ORD); }
"chr"            { return yylval_token (TOK_CHR); }

{IDENT}         { return yylval_token (TOK_IDENT); }
{INTCONST}        { return yylval_token (TOK_INTCON); }
{CHARCONST}        { return yylval_token (TOK_CHARCON); }
{STRINGCONST}   { return yylval_token (TOK_STRINGCON); }

{INVALIDIDENT}  { scanner_badtoken (yytext); }
{UNTERMCHAR}    { scanner_badtoken (yytext); }
{UNTERMSTR}     { scanner_badtoken (yytext); }
.               { scanner_badchar (*yytext); }

%%

