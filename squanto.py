import base64
import json


class squanto:
    def __init__(self, jason=None):
        if jason is None:
            self.jason = {}
            self.data = {}
            self.methods = {}
        else:
            self.jason = json.loads(jason)
            self.data = {}
            self.methods = {}
            for key in self.jason:
                if '_squanto_' in str(self.jason[key]):
                    self.methods[key] = self.jason[key][9:]
                else:
                    self.data[key] = self.jason[key]

    def byteEval(self, method_name, *args):
        interpreter = sqInterpreter(self.data, self.methods[method_name])
        interpreter.run()


class sqInterpreter:
    def __init__(self, json_data, instruc_bytecode):
        self.instructions = base64.standard_b64decode(instruc_bytecode).split(';')
        self.stack = []
        self.json_data = json_data
        self.flags = {'EQ': False,
                      'GT': False, 'GE': False,
                      'LT': False, 'LE': False}
        self.cur_byte_index = 0

    def resetFlags(self):
        for key in self.flags:
            self.flags[key] = False

    def run(self):
        while self.cur_byte_index < len(self.instructions) + 1:
            # ADD
            if self.instructions[self.cur_byte_index][0] == 'A':
                d1 = self.stack.pop()
                d2 = self.stack.pop()
                self.stack.append(d1 + d2)
                self.cur_byte_index += 1
            # SUB
            elif self.instructions[self.cur_byte_index][0] == 'S':
                d1 = self.stack.pop()
                d2 = self.stack.pop()
                self.stack.append(d1 - d2)
                self.cur_byte_index += 1
            # DIV
            elif self.instructions[self.cur_byte_index][0] == 'D':
                d1 = self.stack.pop()
                d2 = self.stack.pop()
                self.stack.append(d1 / d2)
                self.cur_byte_index += 1
            # MUL
            elif self.instructions[self.cur_byte_index][0] == 'M':
                d1 = self.stack.pop()
                d2 = self.stack.pop()
                self.stack.append(d1 * d2)
                self.cur_byte_index += 1
            # PUSH
            elif self.instructions[self.cur_byte_index][0] == 'P':
                self.stack.append(self.json_data[int(self.instructions[self.cur_byte_index][2:])])
                self.cur_byte_index += 1
            # PUSH KONSTANT
            elif self.instructions[self.cur_byte_index][0] == 'K':
                self.stack.append(int(self.instructions[self.cur_byte_index][2:]))
                self.cur_byte_index += 1
            # PUSH ZTRING
            elif self.instructions[self.cur_byte_index][0] == 'Z':
                self.stack.append(self.instructions[self.cur_byte_index][2:])
                self.cur_byte_index += 1
            # PUSH THIS OF ZTRING
            elif self.instructions[self.cur_byte_index][0] == 'T':
                self.stack.append(self.json_data[self.stack.pop()])
            # STORE OF ZTRING
            elif self.instructions[self.cur_byte_index][0] == 'I':
                self.json_data[self.instructions[self.cur_byte_index][2:]] = self.stack.pop()
            # POP
            elif self.instructions[self.cur_byte_index][0] == 'O':
                self.stack.pop()
                self.cur_byte_index += 1
            # JUMP
            elif self.instructions[self.cur_byte_index][0] == 'J':
                self.cur_byte_index += (self.instructions[self.cur_byte_index][2:])
            # CMP
            elif self.instructions[self.cur_byte_index][0] == 'C':
                self.resetFlags()
                L = self.stack.pop()
                R = self.stack.pop()
                if L == R:
                    self.flags['EQ'] = True
                    self.flags['GE'] = True
                    self.flags['LE'] = True
                elif L > R:
                    self.flags['GT'] = True
                    self.flags['GE'] = True
                else:
                    self.flags['LT'] = True
                    self.flags['LE'] = True
                self.cur_byte_index += 1
            # JEQ
            elif self.instructions[self.cur_byte_index][0] == 'E':
                args = self.instructions[self.cur_byte_index].split()
                if self.flags['EQ']:
                    self.cur_byte_index += int(args[1])
                else:
                    self.cur_byte_index += 1
            # J!EQ
            elif self.instructions[self.cur_byte_index][0] == 'N':
                args = self.instructions[self.cur_byte_index].split()
                if not self.flags['EQ']:
                    self.cur_byte_index += int(args[1])
                else:
                    self.cur_byte_index += 1
            # JGT
            elif self.instructions[self.cur_byte_index][0] == 'G':
                args = self.instructions[self.cur_byte_index].split()
                if self.flags['GT']:
                    self.cur_byte_index += int(args[1])
                else:
                    self.cur_byte_index += 1
            # JLT
            elif self.instructions[self.cur_byte_index][0] == 'L':
                args = self.instructions[self.cur_byte_index].split()
                if self.flags['LT']:
                    self.cur_byte_index += int(args[1])
                else:
                    self.cur_byte_index += 1
            # JGEQ
            elif self.instructions[self.cur_byte_index][0] == 'H':
                args = self.instructions[self.cur_byte_index].split()
                if self.flags['GE']:
                    self.cur_byte_index += int(args[1])
                else:
                    self.cur_byte_index += 1
            # JLEQ
            elif self.instructions[self.cur_byte_index][0] == 'X':
                args = self.instructions[self.cur_byte_index].split()
                if self.flags['LE']:
                    self.cur_byte_index += int(args[1])
                else:
                    self.cur_byte_index += 1
            # RETURN
            elif self.instructions[self.cur_byte_index][0] == 'R':
                self.cur_byte_index = len(self.instructions) + 2