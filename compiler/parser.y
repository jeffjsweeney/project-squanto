%{
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "lyutils.h"
#include "astree.h"
#include "astree.rep.h"

#define YYDEBUG 1
#define YYERROR_VERBOSE 1
#define YYPRINT yyprint
#define YYMALLOC yycalloc

static void *yycalloc (size_t size);

%}

%debug
%defines
%error-verbose
%token-table

%token TOK_VOID TOK_BOOL TOK_CHAR TOK_INT TOK_STRING
%token TOK_IF TOK_ELSE TOK_WHILE TOK_RETURN TOK_STRUCT
%token TOK_FALSE TOK_TRUE TOK_NULL TOK_NEW TOK_ARRAY
%token TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE
%token TOK_IDENT TOK_INTCON TOK_CHARCON TOK_STRINGCON
%token TOK_VARDECLINIT TOK_PARAM TOK_INDEX

%token TOK_BLOCK TOK_CALL TOK_IFELSE TOK_DECLID
%token TOK_POS TOK_NEG TOK_NEWARRAY TOK_TYPEID TOK_FIELD TOK_NEWSTRING
%token TOK_ORD TOK_CHR TOK_ROOT TOK_RETURNVOID TOK_FUNCTION

%right TOK_IF 
%right TOK_ELSE '='

%left TOK_EQ TOK_NE TOK_LT TOK_LE TOK_GT TOK_GE '<' '>'
%left '+' '-' 
%left '*' '/' '%'
%right TOK_POS TOK_NEG '!' TOK_ORD TOK_CHR
%left TOK_FIELD TOK_CALL TOK_ARRAY '.' '[' ']'
%nonassoc TOK_NEW
%nonassoc '(' ')'

%start start

%%

start    :  function         { yyparse_astree = $1 }

basetype
    : TOK_INT   { $$ = $1; }
    | TOK_STRING     { $$ = $1; }
    ;
    
function
    : functiondef param ')' block  
    {
        $$ = makefnroot($1, TOK_FUNCTION); 
        
        $$ = adopt2($$, $1, $2);
        $$ = adopt1($$, $4);
        
    }
    | functiondef '(' ')' block           
        { $$ = makefnroot($1, TOK_FUNCTION); $$ = adopt1($$, $1); }
    ;
functiondef	: TOK_VOID TOK_IDENT
			;
param
    : '(' identdecl   
    { changesym($1, TOK_PARAM); $$ = adopt1($1, $2); }
    | param ',' identdecl   { $$ = adopt1($1, $3); }
    ;


identdecl
    : basetype TOK_IDENT 
    { changesym($2, TOK_DECLID); $$ = adopt1($1, $2); }
    
    | basetype TOK_ARRAY TOK_IDENT  
        {  changesym($3, TOK_DECLID); $$ = adopt2($2, $1, $3); }
    | TOK_IDENT TOK_IDENT           
    { changesym($2, TOK_DECLID); $$ = adopt1($1, $2); }
    ;
    
block
    : '{' '}'                
    | statements '}'         { $$ = $1; }
    | ';'                    { $$ = NULL; }                 
    ;

statements
    : '{' statement        
    { changesym($1, TOK_BLOCK); $$ = adopt1($1, $2); }
    | statements statement   { $$ = adopt1($1, $2); }  
    ;

statement
    : block                { $$ = $1; }
    | vardecl            { $$ = $1; }
    | while                { $$ = $1; }
    | ifelse            { $$ = $1; }
    | return            { $$ = $1; }
    | identdecl ';'        { $$ = $1; }
    | expr ';'            { $$ = $1; }
    ;

vardecl
    : identdecl '=' expr ';' { changesym($2, TOK_VARDECLINIT); 
                        $$ = adopt2($2, $1, $3); }
    ;
    
while
    : TOK_WHILE '(' expr ')' statement { $$ = adopt2($1, $3, $5); }
    ;

ifelse
    : TOK_IF '(' expr ')' statement TOK_ELSE statement
    { changesym($1, TOK_IFELSE);
    adopt1($1, $3); $$ = adopt2($1, $5, $7); }
    | TOK_IF '(' expr ')' statement %prec TOK_ELSE  
    { $$ = adopt2($1, $3, $5); }
    ; 
    

return
    : TOK_RETURN ';'            
        { $$ = adopt1sym($1, NULL, TOK_RETURNVOID); }
    | TOK_RETURN expr ';'      { $$ = adopt1($1, $2); }
    ;



expr
    : expr '=' expr             { $$ = adopt2($2, $1, $3); }
    | expr '+' expr             { $$ = adopt2($2, $1, $3); }
    | expr '-' expr             { $$ = adopt2($2, $1, $3); }
    | expr '/' expr             { $$ = adopt2($2, $1, $3); }
    | expr '*' expr             { $$ = adopt2($2, $1, $3); }
    | expr '%' expr             { $$ = adopt2($2, $1, $3); }
    | '!' expr               { $$ = adopt1($1, $2); }
    | TOK_ORD expr            { $$ = adopt1($1, $2); }
    | TOK_CHR expr            { $$ = adopt1($1, $2); }
    | expr TOK_EQ expr         { $$ = adopt2($2, $1, $3); }
    | expr TOK_NE expr         { $$ = adopt2($2, $1, $3); }
    | expr TOK_GE expr         { $$ = adopt2($2, $1, $3); }
    | expr TOK_LE expr         { $$ = adopt2($2, $1, $3); }
    | expr TOK_LT expr         { $$ = adopt2($2, $1, $3); }
    | expr TOK_GT expr         { $$ = adopt2($2, $1, $3); }
    | expr '<' expr             { $$ = adopt2($2, $1, $3); }
    | expr '>' expr             { $$ = adopt2($2, $1, $3); }
    | '+' expr %prec TOK_POS { $$ = adopt1sym ($1, $2, TOK_POS); }
    | '-' expr %prec TOK_NEG { $$ = adopt1sym ($1, $2, TOK_NEG); }   
    | call                     { $$ = $1; }    
    | variable                 { $$ = $1; }    
    | constant                 { $$ = $1; }
    | '(' expr ')'             { $$ = $2; }
    ;

    
call
    : TOK_IDENT call_param ')'       {     $$ = adopt1($2, $1); }
    | TOK_IDENT '(' ')'             
    {  changesym($2, TOK_CALL); $$ = adopt1($2, $1); }
    ;
    
call_param
    : '(' expr              
    { changesym($1, TOK_CALL); $$ = adopt1($1, $2); }
    | call_param ',' expr   { $$ = adopt1($1, $3); }
    ;

variable
    : TOK_IDENT                { $$ = $1; }
    | expr '[' expr ']'     
    { changesym($3, TOK_INDEX); $$ = adopt2($2, $1, $3); }
    | expr '.' TOK_IDENT    
    { changesym($3, TOK_FIELD); $$ = adopt2($2, $1, $3); }
    ;

constant
    : TOK_INTCON    { $$ = $1; }
    | TOK_STRINGCON { $$ = $1; }
    | TOK_CHARCON    { $$ = $1; }
    | TOK_FALSE        { $$ = $1; }
    | TOK_TRUE        { $$ = $1; }
    | TOK_NULL        { $$ = $1; }
    ;
    
%%

const char *get_yytname (int symbol) {
   return yytname [YYTRANSLATE (symbol)];
   
}

static void *yycalloc(size_t size) {
    void *result = calloc(1, size);
    assert (result != NULL);
    return result;
}

