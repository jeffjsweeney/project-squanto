#ifndef __STRINGTABLE_H__
#define __STRINGTABLE_H__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strhash.h"
typedef char *cstring;

typedef struct _stringtable *stringtable_ref;
typedef struct stringnode *stringnode_ref;
stringtable_ref new_stringtable(void);
void delete_stringtable(stringtable_ref table);
void debugdump_stringtable(stringtable_ref table, FILE* outfile);

stringnode_ref intern_stringtable (stringtable_ref, cstring);
cstring peek_stringtable (stringnode_ref);
hashcode_t hashcode_stringtable (stringnode_ref);

#endif
//
