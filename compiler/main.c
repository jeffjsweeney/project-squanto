// $Id: cppstrtok.c,v 1.14 2011-10-03 20:20:08-07 - - $

// Use cpp to scan a file and print line numbers.
// Print out each input line read in, then strtok it for
// tokens.

#if defined ( _WIN32 ) || defined ( _WIN64 )
#define on_windows() 1
#define __WINDOWS__
#define strtok_r strtok_s
#define popen _popen
#define pclose _pclose
#include <io.h>
#include <windows.h>
#define CPP \
    "C:\\Users\\Jeff\\Documents\\Classwork\\CMPS104a\\asg1\\mcpp.exe"

#else
#define _GNU_SOURCE
#include <libgen.h>
#include <wait.h>
#include <unistd.h>
#include "auxlib.h"
#include "lyutils.h"
#define CPP "/usr/bin/cpp"
#define on_windows() 0
#define set_debugflags(flags)
#endif

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "strtable.h"
#include "symtable.h"
#include "codegen.h"


int exit_status = EXIT_SUCCESS;
char *progname;


#define LINESIZE 1024

#ifdef __WINDOWS__
char* basename(char *fullpath)
{
    char _basename[256];
    char _ext[256];
    _splitpath(fullpath, NULL, NULL, _basename, _ext);
    if (on_windows())
    {
        strcat(_basename, _ext);
    }
    char *result = fullpath + (strlen(fullpath) - (strlen(_basename)));
    return result;
}
#endif

// Print a warning after a failed system call.
void syswarn (char *problem) {
   fflush (NULL);
   fprintf (stderr, "%s: %s: %s\n",
            progname, problem, strerror (errno));
   fflush (NULL);
   exit_status = EXIT_FAILURE;
}

// Detect if a file exists or not:
int file_exists(char* filename)
{
#ifdef __WINDOWS__
    int accesstype =0;
#else
    int accesstype = F_OK | R_OK; // the file can be read and it exists.
#endif
    return access(filename, accesstype ) == 0;
}

// Chomp the last character from a buffer if it is delim.
void chomp (char *string, char delim) 
{
   size_t len = strlen (string);
   if (len != 0){
       char* endpos = string + len - 1;
       if (*endpos == delim) *endpos = '\0';
   }
}

// Run cpp against the bison grammar.
void cppparse (FILE *pipe)
{

   yyin = pipe;

   yyparse();
   
}


// The following two functions are inapplicable to Windows systems.
#ifdef __WINDOWS__
#define eprint_status(command, status)
#define eprint_signal(kind, signal)
#endif

#ifndef TESTADT
int main (int argc, char **argv) {
#ifndef __WINDOWS__
    set_execname(argv[0]);
    yy_flex_debug = 0;
    yydebug = 0;
#endif
    progname = basename (argv[0]);
    int exitstatus = EXIT_SUCCESS;
    char* infilename;
    char* infilebasename;
    char  cpparg[256];
    
    if (argc < 2 && !on_windows())
    {
        fprintf(stderr, "Usage: %s infilename\n", progname);
        return EXIT_FAILURE;
    }

    char strtable_outfilename[256];
    char token_outfilename[256];
    char ast_outfilename[256];
    char sym_outfilename[256];
    char oil_outfilename[256];
    
    char infile_ext[128];
    
    #ifdef __WINDOWS__
    infilename = strdup(argv[1]);
    
    infilebasename = basename(argv[1]);
    #else
    opterr = 0;
    for (;;) {
        int option = getopt (argc, argv, "lyD:@:");
        if (option == EOF) break;
        switch (option) 
        {
        case 'l': 
            yy_flex_debug = 1;
            break;
        case 'y':
            yydebug = 1;
            break;
        case '@': 
            set_debugflags (optarg);
            break;
        case 'D':
            strcpy(cpparg, optarg);
            break;
        default:
            fprintf (stderr, "-%c: invalid option\n", optopt);
            exitstatus = 2;
        }
    }
    int fileindex = optind;
    infilename = strdup(argv[fileindex]);
    infilebasename = basename(argv[fileindex]);
    #endif
    
    // test if the file exists before doing anything
    if (!file_exists(infilename))
    {
        fprintf(stderr, "%s: Error: file '%s' does not exist.\n",
            argv[0], infilename);
        return 1;
    }

    // remove the extension
    memset(infile_ext, 0, 128);   
    memcpy(infile_ext, infilename + strlen(infilename) - 3, 3);
       
    // null out the extension
    for (int i = strlen(infilebasename) - 1; i > 0; i--)
    {
        if (infilebasename[i] == '.')
        {
            infilebasename[i] = '\0';
            break;
        }
        
    }   
    
    strcpy(strtable_outfilename, infilebasename);
    strcat(strtable_outfilename, ".str");

    strcpy(token_outfilename, infilebasename);
    strcat(token_outfilename, ".tok");
    
    strcpy(ast_outfilename, infilebasename);
    strcat(ast_outfilename, ".ast");
    
    strcpy(sym_outfilename, infilebasename);
    strcat(sym_outfilename, ".sym");
    
    strcpy(oil_outfilename, infilebasename);
    strcat(oil_outfilename, ".oil");

    FILE* strtable_outfile = fopen(strtable_outfilename, "w");    
    token_outfile = fopen(token_outfilename, "w");
    FILE* ast_outfile = fopen(ast_outfilename, "w");
    
    FILE* sym_outfile = fopen(sym_outfilename, "w");    
    FILE* oil_outfile = fopen(oil_outfilename, "w");

    int cmdlen = strlen (CPP) + 1 + strlen (infilename) + 1;
    char *command = (char*) malloc(cmdlen);
    strcpy (command, CPP);
    strcat (command, " ");
    strcat (command, infilename);

    stringtable = new_stringtable();
    FILE *pipe = popen (command, "r");
    if (pipe == NULL) {
        syswarn (command);
    } else {
        cppparse (pipe);
        int pclose_rc = pclose (pipe);
        eprint_status (command, pclose_rc);
    }
    free(command);
    free(infilename);
    debugdump_stringtable(stringtable, strtable_outfile);
    
    build_symtables(yyparse_astree);
    
    dump_astree(ast_outfile, yyparse_astree);
    
    do_codegen(yyparse_astree, oil_outfile);
    
    fflush(strtable_outfile);
    fclose(strtable_outfile);
    fflush(token_outfile);
    fclose(token_outfile);
    if (on_windows())
        getchar();//stop the console application from instantly closing
    return getexitstatus();
}
#endif 

