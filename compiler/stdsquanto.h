// Squanto Bytecodes

#define SQUANTO_BYTECODE_VERSION_MAJOR	1
#define SQUANTO_BYTECODE_VERSION_MINOR	4

/*
ADD = 'A'		Add the top 2 items on the stack and push result
SUB = 'S'		Subtract the top 2 items on the stack and push result
DIV = 'D'		Divide the top 2 items on the stack and push result
MUL = 'M'		Multiply the top 2 items on the stack and push result
PUSH = 'P'		Push something onto the stack
POP = 'O'		Pop something off the stack
CMP = 'C'		Compare 2 operands
JE = 'E'		Jump if Equal
JNE = 'N'		Jump if Not Equal
JG = 'G'		Jump if Greater
JL = 'L'		Jump if Less 
JGE = 'H'
JLE = 'M'
JR = 'R'		Return to caller
JMP = 'V'		Jumps to a byte offset
THIS = 'T'
ST = 'I'		Pops the top of the stack and sets it to the operand as a json field

deletion code: ccab40f1
*/


// Bytecode definitions
#define SQB_ADD			'A'
#define SQB_SUB			'S'
#define SQB_DIV			'D'
#define SQB_MUL			'M'
#define SQB_PUSH		'P'
#define SQB_PUSH_FCONS	'K'
#define SQB_PUSH_SCONS	'Z'
#define SQB_POP			'O'
#define SQB_CMP			'C'
#define SQB_JE			'E'
#define SQB_JNE			'N'
#define SQB_JG			'G'
#define SQB_JL			'L'
#define SQB_JGE			'H'
#define SQB_JLE			'X'
#define SQB_JR 			'R'
#define SQB_JMP			'J'
#define SQB_THIS		'T'
#define SQB_ST			'I'
