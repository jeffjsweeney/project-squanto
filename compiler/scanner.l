%{
// $Id: scanner.l,v 1.8 2012-10-11 18:22:20-07 - - $

#include <stdlib.h>
#include <stdbool.h>

#include "auxlib.h"
#include "lyutils.h"

#define YY_USER_ACTION  { scanner_useraction (); }
#define IGNORE(THING)   { }

%}

%option 8bit
%option backup
%option debug
%option ecs
%option nodefault
%option nounput
%option perf-report
%option verbose
%option noyywrap


LETTER          [A-Za-z_]
DIGIT           [0-9]
IDENT           ({LETTER}({LETTER}|{DIGIT})*)
FLOATCONST     	(({DIGIT}+)((\.?)({DIGIT}+))?)
STRINGCONST		(\"([^\\"\n]|\\[\\'"0nt])*\")


%%

"#".*           { scanner_include(); }
[ \t]	 	{ IGNORE(white space) }
\n			{ scanner_newline(); }

"="             { return yylval_token ('='); }
"+"             { return yylval_token ('+'); }
"-"             { return yylval_token ('-'); }
"*"             { return yylval_token ('*'); }
"/"             { return yylval_token ('/'); }

"!"          	{ return yylval_token ('!'); }

">="            { return yylval_token (TOK_GE); }
"<="            { return yylval_token (TOK_LE); }
"=="            { return yylval_token (TOK_EQ); }
"<"             { return yylval_token (TOK_LT); }
">"             { return yylval_token (TOK_GT); }
"!="            { return yylval_token (TOK_NE); }
"if"			{ return yylval_token (TOK_IF); }
"else"			{ return yylval_token (TOK_ELSE); }
"while"			{ return yylval_token (TOK_WHILE); }
"return"		{ return yylval_token (TOK_RETURN); }
"("             { return yylval_token ('('); }
")"             { return yylval_token (')'); }
";"             { return yylval_token (';'); }
"["          	{ return yylval_token ('['); }
"]"          	{ return yylval_token (']'); }
"{"          	{ return yylval_token ('{'); }
"}"          	{ return yylval_token ('}'); }
","          	{ return yylval_token (','); }
"."          	{ return yylval_token ('.'); }
"[]"            { return yylval_token (TOK_ARRAY); }

"float"			{ return yylval_token(TOK_INT);  }
"string"		{ return yylval_token(TOK_STRING); }
"def"			{ return yylval_token(TOK_VOID); }

{IDENT}			{ return yylval_token(TOK_IDENT); }
{FLOATCONST}	{ return yylval_token(TOK_INTCON); }
{STRINGCONST}	{ return yylval_token(TOK_STRINGCON); }

.				{ scanner_badchar(*yytext); }

%%
