#ifndef __ATTRIBUTES_H__
#define __ATTRIBUTES_H__
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

typedef unsigned long bitset_t;

#define ATTR_INDEX_VOID      0L
#define ATTR_INDEX_BOOL      1L
#define ATTR_INDEX_CHAR      2L
#define ATTR_INDEX_INT       3L
#define ATTR_INDEX_NULL      4L
#define ATTR_INDEX_STRING    5L
#define ATTR_INDEX_STRUCT    6L
#define ATTR_INDEX_ARRAY     7L
#define ATTR_INDEX_FUNCTION  8L
#define ATTR_INDEX_VARIABLE  9L
#define ATTR_INDEX_FIELD    10L
#define ATTR_INDEX_TYPEID   11L
#define ATTR_INDEX_PARAM    12L
#define ATTR_INDEX_LVALUE   13L
#define ATTR_INDEX_CONST    14L
#define ATTR_INDEX_VREG     15L
#define ATTR_INDEX_VADDR    16L

#define ATTR_VOID        (bitset_t)(1 << ATTR_INDEX_VOID)
#define  ATTR_BOOL       (bitset_t)(1 << ATTR_INDEX_BOOL)
#define  ATTR_CHAR       (bitset_t)(1 << ATTR_INDEX_CHAR)
#define  ATTR_INT        (bitset_t)(1 << ATTR_INDEX_INT)
#define  ATTR_NULL       (bitset_t)(1 << ATTR_INDEX_NULL)
#define  ATTR_STRING     (bitset_t)(1 << ATTR_INDEX_STRING)
#define  ATTR_STRUCT     (bitset_t)(1 << ATTR_INDEX_STRUCT)
#define  ATTR_ARRAY      (bitset_t)(1 << ATTR_INDEX_ARRAY)
#define  ATTR_FUNCTION   (bitset_t)(1 << ATTR_INDEX_FUNCTION)
#define  ATTR_VARIABLE   (bitset_t)(1 << ATTR_INDEX_VARIABLE)
#define  ATTR_FIELD      (bitset_t)(1 << ATTR_INDEX_FIELD)
#define  ATTR_TYPEID     (bitset_t)(1 << ATTR_INDEX_TYPEID)
#define  ATTR_PARAM      (bitset_t)(1 << ATTR_INDEX_PARAM)
#define  ATTR_LVALUE     (bitset_t)(1 << ATTR_INDEX_LVALUE)
#define  ATTR_CONST      (bitset_t)(1 << ATTR_INDEX_CONST)
#define  ATTR_VREG       (bitset_t)(1 << ATTR_INDEX_VREG)
#define  ATTR_VADDR      (bitset_t)(1 << ATTR_INDEX_VADDR)


bitset_t bitset (int attribute_index);
#define __B(att) bitset(att)

void print_attributes (FILE* outfile, bitset_t attributes);


#endif
