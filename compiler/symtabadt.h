#ifndef __SYMTAB_ADT_H__
#define __SYMTAB_ADT_H__

#include "attributes.h"
#include "strtable.h"

typedef struct symboltable *symboltable_ref;
typedef struct st_entry *entry_ref;

symboltable_ref new_symboltable();
void symboltable_add(symboltable_ref table, 
                     stringnode_ref lexinfo,
                     bitset_t attributes);

                     
entry_ref symboltable_getentry(symboltable_ref table, entry_ref prev);
#define symboltable_gethead(table) symboltable_getentry(table, NULL);

bitset_t symboltable_getattributes(entry_ref n);

stringnode_ref symboltable_getlexinfo(entry_ref n);
void print_symboltable(symboltable_ref table, FILE* outfile);

#endif
