#ifndef __SYMTABLE_H__
#define __SYMTABLE_H__

#include <stdio.h>
#include <assert.h>
#include "attributes.h"
#include "astree.rep.h"
#include "symtabadt.h"

void build_symtables(astree root);
extern symboltable_ref *struct_symtables;
extern char** struct_names;
#endif
