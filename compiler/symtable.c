#include "symtable.h"

#include "lyutils.h"
#include "astree.rep.h"

symboltable_ref *struct_symtables; 
char** struct_names;
int current_struct_symtab = 0;

void init_symtables() {
    struct_symtables = (symboltable_ref*) calloc(
        sizeof(symboltable_ref), 
        getstructcount());
    struct_names = (char**)calloc(sizeof(char*), getstructcount());
    
    for ( int i = 0; i < getstructcount(); i++ )
    {
        struct_symtables[i] = new_symboltable(struct_names[i]);
    }
}

int get_struct_index(char* name)
{
    if (struct_names == NULL) return -2;
    
    for (int i = 0; i < getstructcount(); i++) {
        if (strcmp(name, struct_names[i]) == 0)
        {
            return i;
        }
    }
    
    return -1;
}

void build_struct_symtab(astree structroot) {
    if (structroot->first != NULL){
      char* structname = peek_stringtable(structroot->first->lexinfo);
      struct_names[current_struct_symtab] = structname;
      
      astree fieldnode = structroot->first->next;
      while (fieldnode != NULL) {
        stringnode_ref lexinfo = NULL; 
        if (fieldnode->attributes & ATTR_ARRAY) {
            lexinfo = fieldnode->first->next->lexinfo;  
        }
        else lexinfo = fieldnode->first->lexinfo;
        symboltable_add(struct_symtables[current_struct_symtab],
            lexinfo, fieldnode->attributes);
            
        fieldnode = fieldnode->next;
      }
      current_struct_symtab++;
    }
}

bitset_t tok_attributes( int symbol )
{
    bitset_t attributes = 0;
    
    switch (symbol) {
        case TOK_INT:
            attributes |= ATTR_INT;
            break;
        case TOK_CHAR:
            attributes |= ATTR_CHAR;
            break;
        case TOK_BOOL:
            attributes |= ATTR_BOOL;
            break;            
        case TOK_STRING:
            attributes |= ATTR_STRING;
            break;
        case TOK_VOID:
            attributes |= ATTR_VOID;
            break;
        case TOK_ARRAY:
            attributes |= ATTR_ARRAY;
            break;
        case TOK_NULL:
            attributes |= ATTR_NULL | ATTR_CONST;
            break;
        case TOK_TRUE:
        case TOK_FALSE:
            attributes |= ATTR_BOOL | ATTR_CONST;
            break;
        case TOK_STRINGCON:
            attributes |= ATTR_STRING | ATTR_CONST;
            break;
        case TOK_CHARCON:
            attributes |= ATTR_CHAR | ATTR_CONST;
            break;
        case TOK_FIELD:
            attributes |= ATTR_FIELD | ATTR_TYPEID |ATTR_LVALUE;
            break;
        case TOK_VARDECLINIT:
            attributes |= ATTR_VARIABLE | ATTR_LVALUE;
            break;
        case TOK_FUNCTION:
            attributes |= ATTR_FUNCTION;
            break;
        case TOK_PARAM:
            attributes |= ATTR_PARAM | ATTR_VARIABLE;
            break;
        case TOK_NEWARRAY:
            attributes |= ATTR_CONST;
            break;
        case TOK_NEW:
            attributes |= ATTR_CONST;
            break;
        case TOK_STRUCT:
            attributes |= ATTR_STRUCT;
            break;
    }
    
    return attributes;
}

void enter_block() {
    
}

void analyze_node(astree node, int depth) {
    
    node->attributes |= tok_attributes(node->symbol);    
    
    // cases where children need to be updated as well
    if (node->first != NULL) {
        switch (node->symbol) 
        {
            case TOK_VARDECLINIT:
                node->first->attributes |= 
            (tok_attributes(node->first->symbol) | node->attributes);
                node->attributes |= node->first->attributes;            
                break;
            case TOK_PARAM:
                node->first->attributes |= node->attributes;
                break;
            case TOK_VOID:
            case TOK_INT:
            case TOK_CHAR:
            case TOK_STRING:
            case TOK_BOOL:                
                node->first->attributes |= node->attributes;
                if (node->first->next != NULL) 
                    node->first->next->attributes |= 
                        node->first->attributes;
                break;
            case TOK_ARRAY:  {              
                astree child = node->first;
                bitset_t typeattr = tok_attributes(child->symbol);
                
                while (child != NULL) { 
                    child->attributes |= ATTR_ARRAY | typeattr;
                    child = child->next;
                }
                node->attributes |= typeattr;
                
                break;
            }
            
            case TOK_STRUCT:
            {
                // build the symbol table for the struct
                astree child = node->first;
                while (child != NULL) { 
                    analyze_node(child, depth+1);
                    child = child->next;
                }
                build_struct_symtab(node);
                
                break;
            }
            case TOK_BLOCK:
                // create a new symtable for the block
                enter_block();
                break;
        }
    }
}

void analyze_ast_rec(astree root, int depth) {
   astree child = NULL;
   if (root == NULL) return;
   assert (is_astree (root));
   //printf ("%*s%s ", depth * 3, "", peek_stringtable(root->lexinfo));
   analyze_node (root, depth);
   //printf ("\n");
   for (child = root->first; child != NULL; child = child->next) {
      analyze_ast_rec (child, depth + 1);
   }
}

void build_symtables(astree root) {
    init_symtables();
    analyze_ast_rec(root, 0);
}
