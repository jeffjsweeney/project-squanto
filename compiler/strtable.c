#include "strtable.h"
#define TABLE_INIT_LOADSIZE 4095

typedef struct stringnode 
{
    hashcode_t key;
    cstring value;
} stringnode;

typedef struct _stringtable
{
    stringnode_ref *nodearray;
    //sizerecord_node *sizerecord;
    size_t load;
    size_t arraysize;
}  _stringtable;

// Doubles the array size, rehashing as it goes to avoid ghost entries
void smartarraydouble(stringtable_ref table)
{
    stringnode_ref *backup = (stringnode_ref*) 
        malloc(table->arraysize * sizeof(stringnode_ref));

    memcpy(backup, table->nodearray, 
        table->arraysize * sizeof(stringnode_ref));

    int oldsize = table->arraysize;
    int newsize = (oldsize * 2) + 1;
    int newlength = newsize * sizeof(stringnode_ref);

    table->nodearray = (stringnode_ref*) 
        realloc( table->nodearray, newlength);
    memset(table->nodearray, 0, newlength);
    table->load = 0;
    table->arraysize = newsize;

    // Re-add all of the old keys
    for (int i = 0; i < oldsize; i++)
    {        
        if (backup[i] != NULL) // there is something to readd
        {
            intern_stringtable(table, backup[i]->value);
            free(backup[i]->value);
            free(backup[i]);
        }
    }    
    free(backup);
}

stringtable_ref new_stringtable(void)
{
    stringtable_ref table = (stringtable_ref)
        malloc(sizeof(struct _stringtable));
    table->arraysize = TABLE_INIT_LOADSIZE;
    table->load = 0;
    int length = table->arraysize * sizeof(stringnode_ref);
    table->nodearray = (stringnode_ref*) malloc(length);
    memset(table->nodearray, 0, length);

    return table;
}


stringnode_ref intern_stringtable(stringtable_ref table, cstring str)
{
    hashcode_t hashcode = strhash(str);    
    int index = hashcode % (table->arraysize);
    while (table->nodearray[index] != NULL )
    {
        if (strcmp(table->nodearray[index]->value, str) == 0){
            return table->nodearray[index];
        }
        
        index++;
        index = index % table->arraysize;
    }

    stringnode_ref valptr = (stringnode_ref)
        malloc(sizeof(struct stringnode));
    valptr->key = hashcode;
    valptr->value = strdup(str);

    table->nodearray[index] = valptr;
    table->load++;
    // time to array double
    if ( table->load >= (table->arraysize / 2))
    {
        smartarraydouble(table);
    }

    return valptr;
}

void debugdump_stringtable(stringtable_ref table, FILE *outfile)
{
    /*
    printf("StringTable@%08X {\n", table);
    printf("load = %d,\n", table->load);
    printf("arraysize = %d,\n", table->arraysize);
    printf("nodearray = {\n");*/
    for (size_t i = 0; i < table->arraysize; i++)
    {
        if (table->nodearray[i] != NULL) {
            fprintf(outfile, "%8d ", (int)i);
            fprintf(outfile, "%12u \"%s\"\n",
                table->nodearray[i]->key, table->nodearray[i]->value);
        }
        
    }
    //printf("}\n");
}

cstring peek_stringtable(stringnode_ref node)
{
    return node->value;
}

hashcode_t hashcode_stringtable(stringnode_ref node)
{
    return node->key;
}

void delete_stringtable(stringtable_ref table)
{
    for (size_t i = 0; i <table->arraysize; i++)
    {
        if (table->nodearray[i] != NULL)
        {
            stringnode_ref node = table->nodearray[i];
            free(node->value);
            free(table->nodearray[i]);
        }
    }
    free(table->nodearray);
    free(table);
}
