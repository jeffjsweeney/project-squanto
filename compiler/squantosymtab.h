// SquantoSymtab.h
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct squanto_sym *squanto_symtable;

squanto_symtable new_symtable(void);

void bind_entry(squanto_symtable table, char* ident, int offset, int symbol);
int find_entry(squanto_symtable table, char* ident);
int getEntrySymbol(squanto_symtable table, char* ident);
int getEntryOffset(squanto_symtable table, char* ident);

void squantodebugtable(squanto_symtable table);
