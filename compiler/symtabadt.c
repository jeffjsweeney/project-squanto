#include "symtabadt.h"
#include "strtable.h"



typedef struct st_entry {
    stringnode_ref lexinfo;
    bitset_t attributes;
    entry_ref next;
} st_entry;

typedef struct symboltable {
    entry_ref first;
    entry_ref last;
    char* tag;
} symboltable;

symboltable_ref new_symboltable(char* tag) {
    symboltable_ref table = (symboltable_ref)
            malloc(sizeof(struct symboltable));            
    table->first = table->last = NULL;
    table->tag = tag;
    return table;
}

void symboltable_add(symboltable_ref table, 
                     stringnode_ref lexinfo,
                     bitset_t attributes) {
    
    entry_ref node = (entry_ref)malloc(sizeof(struct st_entry));
    node->lexinfo = lexinfo;
    node->attributes = attributes;
    node->next = NULL;
    
    if ( table->first == NULL ) {
        table->first = table->last = node;
    }
    else {
        table->last->next = node;
        table->last = node;
    }
}

entry_ref symboltable_getentry(symboltable_ref table, entry_ref prev) {
    entry_ref retval = NULL;
    
    if ( prev == 0 ) retval = table->first;
    else retval = prev->next;
    
    return retval;
}

bitset_t symboltable_getattributes(entry_ref n) {
    
    return n->attributes;
}

stringnode_ref symboltable_getlexinfo(entry_ref n) {
    
    return n->lexinfo;
}

void print_symboltable(symboltable_ref table, FILE* outfile) {
    fprintf(outfile, "%p {\n", table);
    
    for (entry_ref n = table->first; n != NULL; n = n->next) {
        fprintf(outfile, "\t%s (%X)\n",
            peek_stringtable(n->lexinfo), n->attributes);
    }
    fprintf(outfile, "}\n");
}
