#include "codegen.h"
#include "stdsquanto.h"
#include "squantosymtab.h"

const char* prg_prolog = "#define __OCLIB_C__\n#include \"oclib.oh\"";
symboltable_ref *struct_symtables;
char** struct_names;

char *nodelex(astree node) {return peek_stringtable(node->lexinfo)
    ;}

int assemble_expression(char* buffer, astree node, squanto_symtable symtable);

int is_binop(int symbol) {
    switch (symbol) {
        case TOK_EQ:
        case TOK_NE:
        case TOK_LT:
        case TOK_LE:
        case TOK_GT:
        case TOK_GE:
            return 1;
    }
    return 0;
}

char* get_oil_vardecl_att( bitset_t attributes, char* buffer) {
    char typedecl[32];
   memset(typedecl, 0, 32);
    
   if ( attributes & ATTR_STRING) strcpy(typedecl, "ubyte *");
   if ( attributes & ATTR_CHAR) strcpy(typedecl, "ubyte ");
   if ( attributes & ATTR_INT) strcpy(typedecl, "int ");
   if ( attributes & ATTR_ARRAY) strcat(typedecl, "*");
   strcpy(buffer, typedecl);
   
   return buffer;
}

char* get_oil_vardecl( entry_ref entry, char* buffer ) {

   bitset_t attributes = symboltable_getattributes(entry);
   return get_oil_vardecl_att(attributes, buffer);
}

void emit_struct(FILE* outfile, int index) {

    char declbuffer[256];
    
    fprintf(outfile, "struct s_%s {\n", struct_names[index]);
    symboltable_ref table = struct_symtables[index];
    
    entry_ref curr = symboltable_gethead(table);
    for(; curr != NULL; curr = symboltable_getentry(table, curr)) {
        char mangledid[256];    
        sprintf(mangledid, "f_%s_%s", struct_names[index],
            peek_stringtable(symboltable_getlexinfo(curr)));
    
        fprintf(outfile, "%s%s;\n", 
            get_oil_vardecl(curr, declbuffer), mangledid);
    }
    
    fprintf(outfile, "};\n");
}

void emit_statement(astree node, FILE* outfile) {}

int strcon_count = 0;

void emit_strconstant(astree node, FILE* outfile) {
    if (node->symbol == TOK_STRINGCON) {
        strcon_count++;
        fprintf(outfile, "ubyte *s%i = %s;\n", 
                    strcon_count, 
                    nodelex(node));
    }
}

void emit_strconstant_rec(astree node, FILE* outfile) {
    if (node == NULL) return;
    emit_strconstant(node, outfile);
    
    for (astree child = node->first; child != NULL; 
        child = child->next) {
        emit_strconstant_rec (child, outfile);
    }
}

void emit_globals(astree root, FILE* outfile) {
    astree child = root->first;
    for (; child != NULL; child = child->next) {
    
    if (child->symbol == TOK_VARDECLINIT){ 
        char buffer[256];
        
            if (!(child->attributes & ATTR_STRING) &&
                !(child->attributes & ATTR_CONST)) {
                
            fprintf(outfile, "%s__%s;\n", 
                get_oil_vardecl_att(child->first->attributes, buffer), 
                peek_stringtable(child->first->first->lexinfo));
            
            }       
        }
    }
}



astree getparamnode(astree fnode){
    for (astree child = fnode->first; child !=NULL; child=child->next)
    {
        if (child->symbol == TOK_PARAM) return child->first;
    }
    return NULL;
}
astree getblocknode(astree fnode){
    for (astree child = fnode->first; child !=NULL; child=child->next)
    {
        if (child->symbol == TOK_BLOCK) return child;
    }
    return NULL;
}

void write_fdecl(astree node, FILE* outfile) {
    astree block = getblocknode(node);
    if ( block == NULL ) return;
    
    char pbuff[256];
    fprintf(outfile, "%s __%s (\n",
        nodelex(node->first),
        nodelex(node->first->first));
                    
    astree param = getparamnode(node);
    for (int i = 1; param != NULL; param = param->next){
        fprintf(outfile, "    %s _%i_%c", 
            get_oil_vardecl_att(param->attributes, pbuff), i,
            'a' + i - 1);
        if (param->next != NULL) fprintf(outfile, ",\n");
            i++;
    }
    fprintf(outfile, ")\n");
    
}

int ocmaininit = 0;
int ocmainterminate = 0;
void write_code(astree root, int n, FILE* outfile) {
    astree child = root->first;
    
    for (; child != NULL; child = child->next) {
        if (ocmaininit == 0 && n == 0 
            && child->symbol != TOK_FUNCTION) {
            
            fprintf(outfile, "void __ocmain()\n{\n");
            ocmaininit = 1;
        }
    
        switch (child->symbol) {
            case TOK_FUNCTION:
            {
                write_fdecl(child, outfile);
                astree block = getblocknode(child);
                if (block != NULL) {
                    fprintf(outfile, "{\n");
                    write_code(block, n+1, outfile);
                    fprintf(outfile, "}\n\n");
                }
                break;
            }
            case TOK_IF:
            {
                astree operand = child->first;
                if (is_binop(operand->symbol))
                {                
                    astree left = operand->first;
                    if (left != NULL)
                    {
                        astree right = operand->first->next;
                        if (right != NULL)
                        {
                            fprintf(outfile, "ubyte f%d = ", n);
                            fprintf(outfile, "%s %s %s;", nodelex(left),
                                nodelex(operand), nodelex(right));
                            
                        }                    
                    }
                    
                    fprintf(outfile, "if (!f%d) goto fi_%d_%d_%d;\n",
                      n, child->filenr, child->linenr, child->offset);
                      emit_statement(operand->next, outfile);
                    fprintf(outfile, "fi_%d_%d_%d:;\n",
                        child->filenr, child->linenr, child->offset);
                }
                break;
            }
            case TOK_IFELSE:
            {
                astree operand = child->first;
                if (is_binop(operand->symbol))
                {                
                    astree left = operand->first;
                    if (left != NULL)
                    {
                        astree right = operand->first->next;
                        if (right != NULL)
                        {
                            fprintf(outfile, "ubyte f%d = ", n);
                            fprintf(outfile, "%s %s %s;\n", 
                                nodelex(left),
                                nodelex(operand), nodelex(right));
                            
                        }                    
                    }
                    
                    fprintf(outfile, "if (!f%d) goto fi_%d_%d_%d;\n",
                      n, child->filenr, child->linenr, child->offset);
                      emit_statement(operand->next, outfile);
                    fprintf(outfile, "fi_%d_%d_%d:;\n",
                        child->filenr, child->linenr, child->offset);
                }
                break;
            }
        }
    }
}

#define emit_opcode(opcode)         { char sbuf[128]; sprintf(sbuf,"%c", opcode); strcat(buffer, sbuf); bytecount++; }
#define emit_operand(operand)       { char sbuf[128]; sprintf(sbuf, "%02i", operand); strcat(buffer, sbuf); bytecount++; }
#define emit_operand_s(operand)     { char sbuf[128]; sprintf(sbuf, "%s", operand); strcat(buffer, sbuf); bytecount++; }
#define emit_delim()                { strcat(buffer, " "); }
#define opcode_end()                { strcat(buffer, ";"); }

int block_bytelength(astree node, squanto_symtable symtable)
{
    astree current = node;
    int bytecount = 0;
    char bc_buffer[1024];

    for(; current != NULL; current = current->next)
    {
        assemble_expression(bc_buffer, current, symtable);        
    }
    for (int i = 0; i <strlen(bc_buffer); i++)
    {
        if (bc_buffer[i] == ';')
            bytecount++;
    }
    return bytecount;
}

int assemble_expression(char* buffer, astree node, squanto_symtable symtable)
{
    int bytecount = 0;
    switch (node->symbol)
    {
        case '=': // first: ident, first->next: expression
        {
            astree target = node->first;
            bool is_this_assignment = (target->symbol == '.');

            char* targetident = peek_stringtable(target->lexinfo);
            if (target->symbol != TOK_IDENT && target->symbol != '.') {
                fprintf(stderr, "Error: invalid assignment to non-identifier '%s'\n", targetident);
                exit(1);
            }
            
            if (is_this_assignment){
                char* fieldname = peek_stringtable(target->first->next->lexinfo);
                bytecount += assemble_expression(buffer, target, symtable);
                emit_opcode(SQB_ST);
                emit_delim();
                emit_operand_s(fieldname);
                opcode_end();
                break;
            }

            int data_offset = getEntryOffset(symtable, targetident);
            //printf("Find data offset for identifier %s->%d\n", targetident, data_offset);
            if (data_offset != -1 && !is_this_assignment)
            {
                bytecount += assemble_expression(buffer, node->first->next, symtable);
                emit_opcode(SQB_POP);
                emit_delim();
                emit_operand(data_offset);
                opcode_end();
            }
            
            // assemble children
            break;
        }
        
        case '+':
        {
            astree leftop = node->first;
            astree rightop = node->first->next;

            bytecount += assemble_expression(buffer, leftop, symtable);
            bytecount += assemble_expression(buffer, rightop, symtable);
            
            emit_opcode(SQB_ADD);
            opcode_end();
            break;
        }
        case '-':
        {
            astree leftop = node->first;
            astree rightop = node->first->next;

            bytecount += assemble_expression(buffer, leftop, symtable);
            bytecount += assemble_expression(buffer, rightop, symtable);
            emit_opcode(SQB_SUB);
            opcode_end();   
            break;
        }
        case '*':
        {
            astree leftop = node->first;
            astree rightop = node->first->next;

            bytecount += assemble_expression(buffer, leftop, symtable);
            bytecount += assemble_expression(buffer, rightop, symtable);
            emit_opcode(SQB_MUL);
            opcode_end();
            break;
        }
        case '/':
        {
            astree leftop = node->first;
            astree rightop = node->first->next;

            bytecount += assemble_expression(buffer, leftop, symtable);
            bytecount += assemble_expression(buffer, rightop, symtable);
            emit_opcode(SQB_DIV);
            opcode_end();
            break;
        }
        case TOK_IDENT:
        {
            char *identstr = peek_stringtable(node->lexinfo);
            int offset = getEntryOffset(symtable, identstr);
            if (offset != -1)
            {
                emit_opcode(SQB_PUSH);
                emit_delim();
                emit_operand(offset);
                opcode_end();
            }
            else { fprintf(stderr, "Error: invalid idenfifier: %s\n", identstr); }
            break;
        }
        case TOK_INTCON:
        {
            char *floatstr = peek_stringtable(node->lexinfo);
            emit_opcode(SQB_PUSH_FCONS);
            emit_delim();
            emit_operand_s(floatstr);
            opcode_end();
            break;
        }
        case TOK_STRINGCON:
        {
            char *stringptr = strdup(peek_stringtable(node->lexinfo) + 1);
            stringptr[strlen(stringptr)-1] = 0;

            emit_opcode(SQB_PUSH_SCONS);
            emit_delim();
            emit_operand_s(stringptr);
            opcode_end();

            free(stringptr);
            break;
        }
        case TOK_RETURN:
        {
            emit_opcode(SQB_JR);
            opcode_end();
            break;
        }
        case '.': // this.*
        {
            char* thisstr = peek_stringtable(node->first->lexinfo);
            if (strcmp(thisstr, "this") == 0){
                char *fieldstr = peek_stringtable(node->first->next->lexinfo);
                emit_opcode(SQB_PUSH_SCONS);
                emit_delim();
                emit_operand_s(fieldstr);
                opcode_end();

                emit_opcode(SQB_THIS);
                opcode_end();

            } else {
                fprintf(stderr, "Invalid field access: only 'this' is supported left of a '.'.\n");
                exit(1);
            }
            break;
        }
        case TOK_IFELSE:
        {            
            bytecount += assemble_expression(buffer, node->first, symtable); // assemble the comparison
            int jump_offset = block_bytelength(node->first->next->first, symtable) + 1; // because of the jump added after
            emit_opcode(SQB_CMP);
            opcode_end();
            
            switch(node->first->symbol){
                case TOK_EQ:
                    emit_opcode(SQB_JNE);
                    break;
                case TOK_NE:
                    emit_opcode(SQB_JE);
                    break;
                case TOK_GT:
                    emit_opcode(SQB_JLE);
                    break;
                case TOK_GE:
                    emit_opcode(SQB_JL);
                    break;
                case TOK_LT:
                    emit_opcode(SQB_JGE);
                    break;
                case TOK_LE:
                    emit_opcode(SQB_JG);
                    break;
                default:
                    fprintf(stderr, "Invalid comparison operation. If you are using an identifier, add '!= NULL'\n");
                    exit(1);
                    break;
            }
            emit_delim();
            emit_operand(jump_offset);
            opcode_end();

            bytecount += assemble_expression(buffer, node->first->next, symtable); // the block to execute if true
            jump_offset = block_bytelength(node->first->next->next, symtable);
            emit_opcode(SQB_JMP);
            emit_delim();
            emit_operand(jump_offset);
            opcode_end();
            bytecount += assemble_expression(buffer, node->first->next->next, symtable); // the block to execute if false

            break;
        }
        case TOK_GT: case TOK_LT: case TOK_EQ: case TOK_NE: case TOK_LE: case TOK_GE:
        {
            bytecount += assemble_expression(buffer, node->first, symtable);
            bytecount += assemble_expression(buffer, node->first->next, symtable);
            //emit_opcode(SQB_CMP);
            //opcode_end();
            break;   
        }
        case TOK_BLOCK:
        {
            for (astree current = node->first; current != NULL; current = current->next)
            {
                bytecount += assemble_expression(buffer, current, symtable);
            }
            break;
        }
    }
    return bytecount;
}

void assemble_bytecode(astree codebegin_node, squanto_symtable symtable)
{
    char buffer[4096];
    for (astree expression = codebegin_node; expression != NULL; expression = expression->next)
    {
        assemble_expression(buffer, expression, symtable);
    }
    printf("%s\n", buffer);
}

int write_params(astree param_node, squanto_symtable symtable, int* data_offset)
{    
    int i = 1;
    for (astree param_type = param_node->first; param_type != NULL; param_type = param_type->next)
    {
        if (i > 1)
            printf(",");

        if (param_type->symbol == TOK_INT)
        {
            printf("%d", 0);
            bind_entry(symtable, peek_stringtable(param_type->first->lexinfo), *data_offset, param_type->symbol);
            *data_offset = *data_offset + 1;
            
        }
        else if (param_type->symbol == TOK_STRING)
        {
            printf("\"<P%d>\"", i);
            bind_entry(symtable, peek_stringtable(param_type->first->lexinfo), *data_offset, param_type->symbol);
            *data_offset = *data_offset + 1;
  
        }
        else { return false; } // invalid parameter type
        i++;
    }
    return i - 1;
}

bool is_decl(astree node)
{
    return (node->symbol == TOK_INT || node->symbol == TOK_STRING);
}

int write_localvars(astree node, squanto_symtable symtable, int* data_offset)
{
    astree current = node->first;
    int vCount = 1;
    for (; current != NULL; current = current->next)
    {
        if (current->symbol == TOK_INT) //&& current->first == TOK_DECLID)
        {
            printf("0");
            bind_entry(symtable, peek_stringtable(current->first->lexinfo), *data_offset, current->symbol);
            *data_offset = *data_offset + 1;
            
            if (is_decl(current->next)) // there is another parameter
                printf(",");        
        }
        if (current->symbol == TOK_STRING) //&& current->first == TOK_DECLID)
        {
            printf("\"<V%d>\"", vCount);
            bind_entry(symtable, peek_stringtable(current->first->lexinfo), *data_offset, current->symbol);
            *data_offset = *data_offset + 1;
            
            if (is_decl(current->next)) // there is another parameter
                printf(",");        
        }
        
        vCount ++;

    }
    return vCount;
}

bool gen_symtable(astree root, squanto_symtable symtable)
{
    astree param_node, codeblock_node;
    astree current_node = root->first; // the first child of root
    int data_index = 0; // index into the current data element

    while (current_node != NULL)
    {
        if (current_node->symbol == TOK_PARAM)
            param_node = current_node;
        if (current_node->symbol == TOK_BLOCK)
            codeblock_node = current_node;

        current_node = current_node->next;
    }
    if (param_node == NULL || codeblock_node == NULL)  // bad parse tree, no parameters or codeblock node
        return false;

    printf("[");
    
    int paramcount = write_params(param_node, symtable, &data_index); 
    if (paramcount >= 1){
        printf(",");
    }
    int localvarcount = write_localvars(codeblock_node, symtable, &data_index);

    if (localvarcount == 0)
    {
        printf("\8");
    }
    printf("]");
        

    return false;
}

int do_codegen( astree root, FILE* outfile ) {
    fprintf(outfile, "%s\n\n", prg_prolog); // oc source prolog
    
    if ( struct_symtables != NULL ) {
        for (int i = 0; i < getstructcount(); i++) {
            emit_struct(outfile, i);
        }
    } 
    emit_strconstant_rec(root, outfile);
    emit_globals(root, outfile);
    
    write_code(root, 0, outfile);

    squanto_symtable symtable = new_symtable();

    gen_symtable(root, symtable);
    printf("\n");

    astree current_node = root->first; // the first child of root
    astree codeblock_node = NULL;
    while (current_node != NULL)
    {
        if (current_node->symbol == TOK_BLOCK)
        {
            codeblock_node = current_node->first;
            break;
        }
        current_node = current_node->next;
    }
    while (codeblock_node != NULL && (codeblock_node->symbol == TOK_INT || codeblock_node->symbol == TOK_STRING))
    {
        codeblock_node = codeblock_node->next;
    }

    assemble_bytecode(codeblock_node, symtable);
    printf("\n");

    if ( ocmaininit == 0 ) {
        fprintf(outfile, "void __ocmain ()\n{\n}\n");
        ocmainterminate = 1;

    }
    if ( ocmainterminate == 0 )
    {   
        fprintf(outfile, "}\n");
    }
    
    
    return 0;
}

