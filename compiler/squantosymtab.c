#include "squantosymtab.h"

#define SQUANTO_SYMTABLE_DEFAULT_SIZE	32

typedef struct sym_ent {
	int 	symbol; // holds type information
	char 	ident[32];
	int 	data_offset; // offset into the data[] array
} sym_ent;

typedef struct squanto_sym {
	sym_ent *entries;
	int nEntries;
	int capacity;
} squanto_sym;

squanto_symtable new_symtable(void)
{
	squanto_symtable val = malloc(sizeof(struct squanto_sym));
	val->capacity =	SQUANTO_SYMTABLE_DEFAULT_SIZE;
	val->entries = malloc(sizeof(sym_ent) * val->capacity);
	val->nEntries = 0;
}


bool in_table(squanto_symtable table, char* ident)
{
	for (int i = 0; i < table->nEntries; i++)
	{
		if (strcmp(table->entries[i].ident, ident) == 0)
		{
			return true;
		} 
	}
	return false;
}

void bind_entry(squanto_symtable table, char* ident, int offset, int symbol)
{
	if (!in_table(table, ident))
	{
		//printf("Bind squanto entry: %s->%d\n", ident, offset);
		table->entries[table->nEntries].data_offset = offset;
		table->entries[table->nEntries].symbol = symbol;
		strcpy(table->entries[table->nEntries].ident, ident);
		table->nEntries++;
	}
}

int find_entry(squanto_symtable table, char* ident)
{
	for (int i = 0; i < table->nEntries; i++)
	{
		if (strcmp(table->entries[i].ident, ident) == 0)
		{
			return i;
		} 
	}
	return -1;
}

int getEntrySymbol(squanto_symtable table, char* ident)
{
	int index = find_entry(table, ident);
	if (index != -1)
	{
		return table->entries[index].symbol;
	}
	return -1;
}

int getEntryOffset(squanto_symtable table, char* ident)
{
	int index = find_entry(table, ident);
	if (index != -1)
	{
		return table->entries[index].data_offset;
	}
	return -1;
}

void squantodebugtable(squanto_symtable table)
{
	printf("table %p [%d] {\n", table, table->nEntries);

	for (int i =0; i < table->nEntries; i++)
	{
		printf("\t%s: data[%d] (type=%d)\n", table->entries[i].ident, table->entries[i].data_offset, table->entries[i].symbol);
	}
	printf("}\n");
}
