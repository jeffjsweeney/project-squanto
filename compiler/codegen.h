#ifndef __CODEGEN_H__
#define __CODEGEN_H__

#include <stdio.h>
#include "astree.rep.h"
#include "symtable.h"
#include "lyutils.h"
#include "attributes.h"

int do_codegen( astree root, FILE* outfile ) ;


#endif
