require './squanto.rb'

describe Squanto, '#lol' do
  it 'should return lulz ' do
    squanto = Squanto.new('{"asdf":false}')
    squanto.lol.should eq('lulz')
  end
end
