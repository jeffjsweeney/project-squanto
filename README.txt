Project Squanto

by Scott Hardy, Matthew Medal, and Jeffrey Sweeney
AngelHack SV 6/16/2013

Overview-

Squanto is an extension of JSON functionality that maintains compatibility with current JSON standards. SquantoJSON parses into valid JSON if a Squanto parser is not used.
Squanto has Lua/C like syntax and functionality for manipulating its parent JSON object, and can perform functions on both data passed in and data within the JSON object.
This allows API providers to bake in functions for their clients to avoid unnecessarily reinventing the wheel. Squanto can be compiled into bytecode and encoded in Base64 to
maintain a low size and a fast, responsive runtime.

Details
Included is a squanto bytecode compiler (as is, it has limited functionality of manipulating fields in the JSON object, and basic codeflow operations). Squanto code can be included in
JSON objects returned by your REST API by simplying adding a key-value pair of the form: "function_name" : "_squanto <base64 compiled squanto bytecode>". Also included is a Python implementation
of a Squanto parser and runtime. SquantoJS is an experimental implementation for JavaScript, also included.