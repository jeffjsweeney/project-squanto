class Squanto
  constructor: (@_squanto) ->
    # parse json
    @_squanto = JSON.parse @_squanto

    # add json data to squanto object
    for a, b of @_squanto
      this[a] = b

    # check for squanto methods
    if this.find_squanto_methods() is false
      console.log 'no squanto methods'




  find_squanto_methods: ->
    @_squanto._squanto_methods = []
    for key, value of @_squanto when typeof(value) is 'string'
      #console.log 'testing ' + key
      if value.substr(0, 8) is '_squanto'
        @_squanto._squanto_methods.push key
    return @_squanto._squanto_methods.length > 0




  flags = {
    EQ: false,
    GT: false,
    GE: false,
    LT: false,
    LE: false
  }
  reset_flags: ->
    for flag, val of flags
      flags[flag] = false








  runMethod: (squanto_method) ->
    bytecode = @_squanto[squanto_method].substr(9)
    bytecode = this.decode bytecode
    bytecode = bytecode.split(';')
    console.log bytecode

    stack = []
    data = @_squanto.data
    index = 0

    while index < bytecode.length
      bytes = bytecode[index].split(' ')
      instruction = bytes[0]
      console.log 'instruction: ' + instruction

      # pushes
      stack.push this.data[bytes[1]] if instruction is 'P'
      stack.push parseFloat(bytes[1]) if instruction is 'K'
      stack.push bytes[1] if instruction is 'Z'

      # +-*/
      if ['A', 'S', 'M', 'D'].indexOf(instruction) isnt -1
        n1 = stack.shift()
        n2 = stack.shift()
        stack.unshift(n1 + n2) if instruction is 'A'
        stack.unshift(n1 - n2) if instruction is 'S'
        stack.unshift(n1 * n2) if instruction is 'M'
        stack.unshift(n1 / n2) if instruction is 'D'

      # pop
      if instruction is 'O'
        stack.pop()

      # json data fetch
      if instruction is 'T'
        a = stack.pop()
        stack.push this[a]

      # json data push
      if instruction is 'I'
        a = stack.pop()
        this[bytes[1]] = a

      # compare
      if instruction is 'C'
        this.reset_flags()
        a = data[bytes[1]]
        b = data[bytes[2]]
        if a is b
          flags.EQ = true
          flags.GE = true
          flags.LE = true
        else if a > b
          flags.GT = true
        else
          flags.LT = true
        console.log flags

      # jumps
      if ['J', 'R', 'E', 'N', 'G', 'L', 'H', 'X'].indexOf(instruction) isnt -1
        console.log 'here'
        index += parseInt(bytes[1]) if instruction is 'J'
        index = bytecode.length if instruction is 'R'
        index += parseInt(bytes[1]) if flags.EQ and instruction is 'E'
        index += parseInt(bytes[1]) if flags.EQ and instruction is 'N'
        index += parseInt(bytes[1]) if flags.GT and instruction is 'G'
        index += parseInt(bytes[1]) if flags.LT and instruction is 'L'
        index += parseInt(bytes[1]) if flags.GE and instruction is 'H'
        index += parseInt(bytes[1]) if flags.LE and instruction is 'X'
      else index++

      console.log stack
    return stack[0]





  decode: (data) ->
    charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    end = "="

    output = ''
    buffer = ''

    for char in data
      # stop if end character
      break if char is end

      # get index of current char
      char_int = charset.indexOf(char)
      throw char + " is not base64!" if char_int is -1

      # convert index to binary
      char_bin = char_int.toString(2) # convert int to base 2 string
      char_bin = "0" + char_bin while char_bin.length < 6 # pad front with 0s
      buffer += char_bin # add binary string to buffer

      while buffer.length >= 8
        octet = buffer.slice(0, 8) # get first 8 characters
        buffer = buffer.slice(8) # remove first 8 characters
        output += String.fromCharCode(parseInt(octet, 2)) # add character from 8-bit
    return output



# add
json = '{"jank1":1, "jank2":2, "data":[2,10], "notsquanto":"fartttttt", "addJank":"_squanto_UCAwO1AgMTtB", "multiplyJank":"_squanto_UCAwO1AgMTtN"}'
test1 = new Squanto(json)
res = test1.runMethod('addJank')
console.log res
console.log res is 12

res = test1.runMethod('multiplyJank')
console.log res
console.log res is 20
